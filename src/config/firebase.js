import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAxYPUuCfe9tZRkzIAii6nL3iwxrnOLs5Y",
  authDomain: "chousei-firebase-ada63.firebaseapp.com",
  databaseURL: "https://chousei-firebase-ada63.firebaseio.com",
  projectId: "chousei-firebase-ada63",
  storageBucket: "",
  messagingSenderId: "410227214456",
  appId: "1:410227214456:web:0e03310761d3afebde058d"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
